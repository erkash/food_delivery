<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setRoles(['ROLE_ADMIN'])
            ->setUsername('admin')
            ->setEnabled(true)
            ->setEmail('original@some.com');
        $password = $this->container
                         ->get('security.password_encoder')
                         ->encodePassword($user, '123321'); //шифруем пароль
        $user->setPassword($password);

        $manager->persist($user);

        $manager->flush();
    }
}
